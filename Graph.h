//
// Created by seanrdev on 11/14/17.
//

#ifndef GRAPHPROJECT_GRAPH_H
#define GRAPHPROJECT_GRAPH_H


#include <vector>

class Graph {
private:
    unsigned int number_of_routes;
    std::vector<unsigned int> *station_vector;
public:
    Graph(unsigned int routes, unsigned int stations);
    void addRoute(unsigned int from, unsigned int to, unsigned int weight);
    bool isRoute(unsigned int from, unsigned int to);
};


#endif //GRAPHPROJECT_GRAPH_H
