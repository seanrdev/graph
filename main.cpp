#include <iostream>
#include <fstream>
#include <string>
#include "Graph.h"
#include <regex>

using namespace std;

int main() {
    std::cout << "Hello, World!" << std::endl;
    ifstream my_file;
    my_file.open("/home/seanrdev/input.txt");
    string input;
    bool first_line = true;
    Graph *g;
    while(getline(my_file, input)) {
        if(first_line == true){
            regex rgx("([0-9]+) ([0-9]+)");
            smatch matches;
            if(regex_search(input, matches, rgx)) {
                g = new Graph(stoi(matches[2].str()), stoi(matches[1].str()));
                first_line = false;
                continue;
            }
        }
        regex rgx("([0-9]+) ([0-9]+) ([0-9]+)");
        smatch matches;
        if(regex_search(input, matches, rgx)) {
            g->addRoute(stoi(matches[1].str()), stoi(matches[2].str()), stoi(matches[3].str()));
            continue;
        }
    }
    return 0;
}