//
// Created by seanrdev on 11/16/17.
//

#include "station.h"


station::station(unsigned int station_id) {
    this->single_route = 0;
    this->station_id = station_id;
}

station *station::get_pointing_to(unsigned int index) {
    return this->to_from.at(index);
}

unsigned int station::getWeight(unsigned int index) {
    return this->weight.at(index);
}

void station::add_pointing_to(station *s) {
    this->to_from.push_back(s);
    this->single_route++;
}

void station::addWeight(unsigned int i) {
    this->weight.push_back(i);
}

void station::increase_index() {
    this->single_route++;
}
