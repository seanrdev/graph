//
// Created by seanrdev on 11/14/17.
//

#include <vector>
#include "Graph.h"
#include "station.h"


Graph::Graph(unsigned int routes, unsigned int stations) {
    station_vector = new std::vector<unsigned int>(stations);
    this->number_of_routes = routes;
}

void Graph::addRoute(unsigned int from, unsigned int to, unsigned int weight) {
    station *from_station = new station(from);
    station *to_station = new station(to);
    from_station->add_pointing_to(to_station);
    to_station->add_pointing_to(from_station);
    from_station->increase_index();
    to_station->addWeight(weight);
    from_station->addWeight(weight);
}

bool Graph::isRoute(unsigned int from, unsigned int to) {
    //run algorithm to determine if route exists from "from" to "to"
    return false;
}
